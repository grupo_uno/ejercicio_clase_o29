package com.app.web.servicios;

import java.util.List;

import com.app.web.entidades.Cliente;

public interface ClienteServicio {
	
	public List<Cliente> listarClientes(); 
}
