package com.app.web.entidades;

import javax.persistence.*;

@Entity
@Table(name="direcciones")
public class Direccion {
	//ATRIBUTOS 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDireccion; 
	@Column(name="calleDireccion", nullable=false, length=10)
	private String calleDireccion; 
	@Column(name="carreraDireccion", nullable=false, length=10)
	private String carreraDireccion; 
	@Column(name="detalleDireccion", nullable=false, length=20)
	private String detalleDireccion;
	
	@OneToOne(mappedBy="direccionCliente")
	private Cliente direccionCliente; 
	
	//CONSTRUCTORES 
	public Direccion() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Direccion(int idDireccion, String calleDireccion, String carreraDireccion, String detalleDireccion,
			Cliente cliente) {
		super();
		this.idDireccion = idDireccion;
		this.calleDireccion = calleDireccion;
		this.carreraDireccion = carreraDireccion;
		this.detalleDireccion = detalleDireccion;
		this.direccionCliente = cliente;
	}

	//GETTER AND SETTER - ENCAPSULAMIENTO 
	public int getIdDireccion() {
		return idDireccion;
	}

	public void setIdDireccion(int idDireccion) {
		this.idDireccion = idDireccion;
	}

	public String getCalleDireccion() {
		return calleDireccion;
	}

	public void setCalleDireccion(String calleDireccion) {
		this.calleDireccion = calleDireccion;
	}

	public String getCarreraDireccion() {
		return carreraDireccion;
	}

	public void setCarreraDireccion(String carreraDireccion) {
		this.carreraDireccion = carreraDireccion;
	}

	public String getDetalleDireccion() {
		return detalleDireccion;
	}

	public void setDetalleDireccion(String detalleDireccion) {
		this.detalleDireccion = detalleDireccion;
	}

	public Cliente getCliente() {
		return direccionCliente;
	}

	public void setCliente(Cliente cliente) {
		this.direccionCliente = cliente;
	}

	//SOBREESCRIBIR EL METODO TOSTRING 
	@Override
	public String toString() {
		return "Direccion [idDireccion=" + idDireccion + ", calleDireccion=" + calleDireccion + ", carreraDireccion="
				+ carreraDireccion + ", detalleDireccion=" + detalleDireccion + ", cliente=" + direccionCliente + "]";
	}

	
}
