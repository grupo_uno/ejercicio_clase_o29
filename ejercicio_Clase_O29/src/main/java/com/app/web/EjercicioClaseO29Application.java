package com.app.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioClaseO29Application {

	public static void main(String[] args) {
		SpringApplication.run(EjercicioClaseO29Application.class, args);
	}

}
